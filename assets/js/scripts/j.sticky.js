;(function(context) {

	var debounce;
	
	if(context) {
		debounce = context.debounce;
	} else {
		debounce = require('./debounce.js');
	}

	var
		d = debounce(),
		$window = $(window),
		updateRequired = false,
		cachedStickies = null,
		STICKY_RELATIVE_KEY = 'STICKY_RELATIVE',
		methods = {
		
			getStickies: function() {
				var self = this;
				
				if(updateRequired || !cachedStickies) {
					cachedStickies = $('.j-sticky').filter(function() {
						var 
							shouldBeSticky = true,
							$this = $(this),
							$parent = $this.parent(),
							data = $this.data(),
							relative = data.stuckTo ? $(data.stuckTo).get(0) : document.body;
							
						if(!$parent.hasClass('j-sticky-wrap')) {
							$this.wrap('<div class="j-sticky-wrap"/>');
						}
						
						$this.data(STICKY_RELATIVE_KEY,relative);
						shouldBeSticky = typeof data.unstickAt === 'undefined' || window.innerWidth > data.unstickAt;
						
						if(!shouldBeSticky) {
							self.updateElWithOffset($this,0);
						}
						
						return shouldBeSticky;
					});
				}
				return cachedStickies;
			},
			
			updateElWithOffset: function($el,offset) {
				$el.css({
					'-webkit-transform':'translate3d(0,'+ offset +'px,0)',
					'-moz-transform':'translate3d(0,'+ offset +'px,0)',
					'-ms-transform':'translate3d(0,'+ offset +'px,0)',
					'transform':'translate3d(0,'+ offset +'px,0)'
				});
			},
		
			updateStickies: function() {
				var self = this;
				this
					.getStickies()
					.each(function(i,el) {
						var 
							$el = $(el),
							relative = $el.data(STICKY_RELATIVE_KEY),
							r = $el.parent().get(0).getBoundingClientRect(),
							offset = r.top < 0 ? Math.abs(r.top) : 0;
							
						if(window.scrollY + r.height >= relative.offsetTop + relative.scrollHeight) { 
							offset = relative.scrollHeight - r.height;
						}
						
						self.updateElWithOffset($el,offset);
						
					});
			}
		};
	
	//can be triggered when needed (e.g. in the case of AJAX updates to the dom)
	//via $(document).trigger('updateTemplate.lazyimages'); or just $(document).trigger('updateTemplate');
	$window.on('scroll load resize updateTemplate.sticky',function(e) { 
		if(e.type !== 'scroll') {
			updateRequired = true;
		} else {
			updateRequired = false;
		}
		d.requestProcess(function() { methods.updateStickies(); }); 
	});

	//no public API
	return {};
	
}(typeof ns !== 'undefined' ? window[ns] : undefined));