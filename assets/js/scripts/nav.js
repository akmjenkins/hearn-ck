;(function(context) {

	var debounce,tests;

	if(context) {
		debounce = context.debounce;
		tests = context.tests;
	} else {
		debounce = require('./debounce.js');
		tests = require('./tests.js');
	}

	var 
		methods,
		scrollDebounce = debounce(),
		resizeDebounce = debounce(),
		$window = $(window),
		$document = $(document),
		$nav  =$('.nav-wrap'),
		$html = $('html'),
		SHOW_NAV_CLASS = 'show-nav';

	methods = {
	
		showNav: function(show) {
			if(show) {
				$html.addClass(SHOW_NAV_CLASS);
			} else {
				$html.removeClass(SHOW_NAV_CLASS);
			}

		},

		toggleNav: function() {
			this.showNav(!this.isShowingNav());
		},

		isShowingNav: function() {
			return $html.hasClass(SHOW_NAV_CLASS);
		}

	};
	
	//listeners
	$document
		.on('click','body',function(e) {
			$(e.target).hasClass('mobile-nav-bg') && methods.showNav(false);
		})		
		.on('click','.toggle-nav',function(e) {
			methods.toggleNav();
			return false;
		})
		.on('click','.toggle-search',function() {
			methods.toggleSearch()
		})
		.on('keydown',function(e) {
			if(e.result !== false && e.keyCode === 27) {
				
				if(methods.isShowingNav()) {
					methods.showNav(false);
					return false
				}
			}
		});
	
}(typeof ns !== 'undefined' ? window[ns] : undefined));