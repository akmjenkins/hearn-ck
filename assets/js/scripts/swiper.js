;(function(context) {
	
	var 
		lazyImageSwipers,
		$window,
		$document,
		debounce,
		d,
		methods,
		imageLoader,
		tests;
	
	if(context) {
		imageLoader = context.imageLoader;
		tests = context.tests;
		debounce = context.debounce;
	} else {
		imageLoader = require('./image.loader.js');
		tests = require('./tests.js');
		debounce = require('./debounce');
	}
	
	lazyImageSwipers = [];
	d = debounce();
	$document = $(document);
	$window = $(window);
	
	methods = {
		
		getElementWithSrcData: function(el) {
			return el.data('src') !== undefined ? el : el.find('.swipe-item-bg').filter(function() { return $(this).data('src') !== undefined });
		},
		
		setImageOnElements: function(els,source) {
			els.each(function() {
				$(this)
					.css({backgroundImage: 'url('+source+')' })
					.addClass('loaded');
			});
		},
		
		loadImageForElementAtIndex: function(i,el) {
			var 
				self = this,
				element = $('.swipe-item',el).filter(function() { return !$(this).hasClass('slick-cloned'); }).eq(i),
				sourceElement = this.getElementWithSrcData(element),
				rawSource = sourceElement.data('src'),
				source = imageLoader.getAppropriateSource(rawSource),
				allElements = sourceElement.add(self.getElementWithSrcData(element.siblings()).filter(function() { 
					return $(this).data('src') === rawSource; 
				}));
			
				element.addClass('loading');
				
				if(!source) {
					element.addClass('loaded');
				}
				
				if(imageLoader.hasSourceLoaded(source)) {
					this.setImageOnElements(allElements,source);
					return;
				}
				
				imageLoader
					.loadSource(source)
					.then(function(source) {
						self.setImageOnElements(allElements,source);
						element.addClass('loaded');
					});
			
		},
	
		getSwiperWrappers: function() {
			return $('div.swiper-wrapper');
		},
		
		updateLazyImagesOnAll: function() {
			var self = this;
			lazyImageSwipers.forEach(function(swiper) { self.updateLazyImagesOnSwiper(swiper); });
		},
		
		updateLazyImagesOnSwiper: function(swiper) {
			for(var i = 0; i< (swiper.slick('slickGetOption','slidesToShow')+1); i++) {
				methods.loadImageForElementAtIndex(swiper.slick('slickCurrentSlide')+i,swiper);	
			}
		},
	
		buildSwiper: function(wrapper,options) {
			
			var 
				settings = options || {},
				selectedClass = settings.selectedClass || 'selected',
				el = wrapper,
				swiper = el.children('div.swiper'),
				selectNav = el.find('select.swiper-nav'),
				navEls = el.find('.slick-move-to'),
				responsive = swiper.data('responsive');
				
				if(swiper.data().fade === 'detect') {
					swiper.data('fade',!tests.touch());
				}
				
				$.extend(settings,swiper.data());

			settings.slidesToShow = settings.slidesToShow || 1;

			//setup listeners
			swiper
				.on('init',function(e,slick) {
					navEls.eq(slick.getCurrent()).addClass(selectedClass);
				})
				.on('beforeChange',function(e,slick,current,next) {					
					if(settings.updateLazyImages) {
						for(var i = 0; i < (swiper.slick('slickGetOption','slidesToShow')+1); i++) {
							methods.loadImageForElementAtIndex(next+i,swiper);	
						}
					}

					navEls
						.removeClass(selectedClass)
						.eq(next)
						.addClass(selectedClass);

				});

			selectNav.on('change',function() { swiper.slick('goTo',this.selectedIndex); });
			navEls.on('click',function() { swiper.slick('goTo',navEls.index(this)); });
				
			//slick it
			el.data('slick',swiper.slick(settings));

			if(settings.updateLazyImages) {
				lazyImageSwipers.push(swiper);
				this.updateLazyImagesOnSwiper(swiper);
			}
			
		},
		
		updateTemplate: function() {
			methods
				.getSwiperWrappers()
				.filter(function() {
					return typeof $(this).data('slick') === 'undefined'
				})
				.each(function() {
					methods.buildSwiper($(this));
				});		
		}
	
	}
	
	$document
		.on('updateTemplate.swiper ready',function(e) {
			methods.updateTemplate();
		});
		
	$window
		.on('resize',function() {
			d.requestProcess(function() { methods.updateLazyImagesOnAll(); });
		});
		
	//CommonJS
	if(typeof module !== 'undefined' && module.exports) {
		module.exports = {
			buildSwiper: function(wrapper,options) { methods.buildSwiper(wrapper,options); }
		}
	//CodeKit
	} else if(context) {
		context.swiper = {
			buildSwiper: function(wrapper,options) { methods.buildSwiper(wrapper,options); }
		}
	}	

}(typeof ns !== 'undefined' ? window[ns] : undefined));