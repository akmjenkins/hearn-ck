//rather than using Modernizr, which is big and bulky and unnecessary seeing how we only support modern browsers now anyway
;(function(context) {

	var $html = $('html');
	
	var cachedResults = {};

	var tests = {
	
		touch: function() {
			
			if(typeof cachedResults.touch === 'undefined') {
				cachedResults.touch = (('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch);
			}
			
			return cachedResults.touch;
			
		},
	
		ios: function() {
			
			if(typeof cachedResults.ios === 'undefined') {
				cachedResults.ios = window.navigator.userAgent.match(/iphone|ipad/i);
			}
			
			return cachedResults.ios;
		},
		
		viewportunits: function() {
			var width,el;
			
			if(typeof cachedResults.viewportunits === 'undefined') {
				width =  window.innerWidth;
				el = document.createElement('div');
				el.style.position = 'absolute';
				el.style.visibility = 'hidden';
				el.style.width = '50vw';
				document.body.appendChild(el);
				cachedResults.viewportunits = (el.offsetWidth === window.innerWidth/2);
				document.body.removeChild(el);
				el = null;
			}
			return cachedResults.viewportunits;
			
		},
		
		dateinputs: function() {
			var el;
			if(typeof cachedResults.dateinputs === 'undefined') {
				el = document.createElement('input');
				el.setAttribute('type','date');
				cachedResults.dateinputs = el.type !== 'text';
				el = null;
			}
			
			return cachedResults.dateinputs;
		}
	};

	//add these to the HTML tag, like modernizr would
	$.each(tests,function(name,cb) {
		$html.addClass(cb() ? name : 'no-'+name);
	});
	
	if(typeof module !== 'undefined' && module.exports) {
		module.exports = tests;
	} else if(context) {
		context.tests = tests;
	}

}(typeof ns !== 'undefined' ? window[ns] : undefined));