;(function(context) {

	var tests;
	var debounce;
	var imageLoader;
	var d;
	var swiper;
	
	if(context) {
		debounce = context.debounce;
		tests = context.tests;
		imageLoader = context.imageLoader;
		swiper = context.swiper;
	} else {
		debounce = require('./scripts/debounce.js');
		tests = require('./scripts/tests.js');
		imageLoader = require('./scripts/image.loader.js');
		swiper = require('./scripts/swiper.js');
	}
	
	d = debounce();
	
	//tooltips
	(function() {
		var methods = {
			
			getElements: function() {
				return $('.tooltip');
			},
			
			doTooltips: function() {
				this.getElements().each(function() {
					var 
						$el = $(this),
						d = $el.data();
					
					$el.data('hasTooltip') && $el.tooltipster('destroy');
					
					if(d.destroyAt && window.innerWidth < d.destroyAt) { return; }
					
					$el.tooltipster(d);
					$el.data('hasTooltip',true);
				});
				
			}
		};
		
		$(document).on('ready updateTemplate.tooltip',function() { methods.doTooltips(); })
		$(window).on('resize',function() { d.requestProcess(function() { methods.doTooltips(); }); })
	}());
	
}(typeof ns !== 'undefined' ? window[ns] : undefined));