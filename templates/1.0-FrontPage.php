<?php $bodyclass = 'home'; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero">
	
	<div class="swiper-wrapper">
		<div class="swiper hero-swiper"
			data-arrows="false" 
			data-autoplay="true"
			data-autoplay-speed="7000"
			data-pause-on-hover="false"
			data-update-lazy-images="true" 
			data-fade="true">

			<div class="swipe-item">

				<div class="swipe-item-bg" data-src="../assets/images/temp/hero/hero-home.jpg"></div>

				<div class="hero-content-wrap">
					<div class="hero-content">

						<h2 class="hero-content-title">Originality</h2>			
						<p>
							Donec dictum libero neque, sit amet semper velit dictum nec. Phasellus ac egestas orci. 
							Cras eu mauris feugiat, suscipit velit eget, ullamcorper ipsum libero neque, sit amet semper.
						</p>

						<a href="#explore-modal" data-src="#explore-modal" data-type="inline" class="mpopup hero-content-link t-fa fa-th">Explore</a>

					</div><!-- .hero-content -->
				</div><!-- .sw -->

			</div><!-- .swipe-item -->

		</div><!-- .swiper -->
	</div><!-- .swiper-wrapper -->

</div><!-- .hero -->

<div class="body">

	<div class="explore-modal-wrap">
		<div class="explore-modal" id="explore-modal">

			<button class="explore-modal-close mpopup-close">Close</button>

			<div class="ov-grid grid nopad">

				<div class="col col-2 sm-col-1">
					<!-- THE CLASS h2 MEANS IT'S "double height" -->
					<a href="#" class="item ov-item h2 bounce">
						<div class="lazybg ov-item-bg img" data-src="../assets/images/temp/menu/block-1.jpg"></div>
						<span>Work</span>
					</a><!-- .ov-item -->
				</div><!-- .col -->

				<div class="col col-2 sm-col-1">
					<a href="#" class="item ov-item h2 bounce">
						<div class="lazybg ov-item-bg img" data-src="../assets/images/temp/menu/block-2.jpg"></div>
						<span>About</span>
					</a><!-- .ov-item -->					
				</div><!-- .col -->

				<div class="col col-4 sm-col-2 xs-col-1">
					<!-- THE CLASS h1 MEANS IT'S "single height" -->
					<a href="#" class="item ov-item h1 bounce">
						<div class="lazybg ov-item-bg img" data-src="../assets/images/temp/menu/sm-block-1.jpg"></div>
						<span>#JHADesign</span>
					</a><!-- .ov-item -->					
				</div><!-- .col -->

				<div class="col col-4 sm-col-2 xs-col-1">
					<a href="#" class="item ov-item h1 bounce">
						<div class="lazybg ov-item-bg img" data-src="../assets/images/temp/menu/sm-block-2.jpg"></div>
						<span>News</span>
					</a><!-- .ov-item -->
				</div><!-- .col -->

				<div class="col col-4 sm-col-2 xs-col-1">
					<a href="#" class="item ov-item h1 bounce">
						<div class="lazybg ov-item-bg img" data-src="../assets/images/temp/menu/sm-block-3.jpg"></div>
						<span>Careers</span>
					</a><!-- .ov-item -->
				</div><!-- .col -->

				<div class="col col-4 sm-col-2 xs-col-1">
					<a href="#" class="item ov-item h1 bounce">
						<div class="lazybg ov-item-bg img" data-src="../assets/images/temp/menu/sm-block-4.jpg"></div>
						<span>Location</span>
					</a><!-- .ov-item -->
				</div><!-- .col -->					
			</div><!-- .ov-grid -->

		</div><!-- .explore-modal -->
	</div><!-- .explode-modal-wrap -->

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>