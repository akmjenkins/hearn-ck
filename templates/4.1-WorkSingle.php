<?php $bodyclass = 'work-single'; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero">
	<div class="lazybg hero-full-bg" data-src="../assets/images/temp/hero/header-airport.jpg"></div>

	<div class="sw">
		<div class="hero-content">

			<h1 class="hero-content-title">St. John's International Airport</h1>			
			<p>
				Donec dictum libero neque, sit amet semper velit dictum nec. Phasellus ac egestas orci. 
				Cras eu mauris feugiat, suscipit velit eget, ullamcorper ipsum libero neque, sit amet semper.
			</p>

		</div><!-- .hero-content -->
	</div><!-- .sw -->

</div><!-- .hero -->

<div class="body">

	<section class="nopad">
		<div class="grid eqh nopad collapse-1000">
			<div class="col-3-5 col light-bg">
				<div class="item">

					<div class="pad-40 sm-pad-20">
						<h4>Where modern design meets function</h4>

						<p>
							As the main gateway to Newfoundland and Labrador, the air terminal building at the SJIA is often the 
							first and last space travelers to the province experience. As such, it was important that the 
							redeveloped and expanded air terminal building convey the energy and dynamism of Newfoundland and 
							Labrador’s resurgent economy, while expressing the province’s unique heritage. Passengers arriving 
							in Newfoundland and Labrador’s new air terminal are greeted with a modern facility incorporating the 
							latest technology in ticketing, baggage handling, security and passenger conveniences and comfort.						
						</p>
					</div><!-- .pad-40 -->

				</div><!-- .item -->
			</div><!-- .col -->
			<div class="col col-2-5 dark-bg">
				<div class="item">
					
					<div class="swiper-wrapper">

						<div class="specifications-control-holder">
							
							<span class="specifications-control-title">Specifications</span>

							<div class="specifications-controls">
								<span class="slick-move-to">Size</span>
								<span class="slick-move-to">Location</span>
								<span class="slick-move-to">Completion</span>
							</div><!-- .specifications-controls -->

						</div><!-- .specifications-control-holder -->

						<div class="swiper"
							data-arrows="false">

							<div class="swipe-item">
								<div class="pad-20">
									<img src="../assets/images/temp/airport.png" alt="st. john's airport" class="aligncenter">

									<p>
										Donec dictum libero neque, sit amet semper velit dictum nec. Phasellus ac egestas orci. 
										Cras eu mauris feugiat, suscipit velit eget, ullamcorper ipsum  libero neque, sit amet semper. 
										Donec dictum libero neque, sit amet semper velit dictum nec. Phasellus ac egestas orci. 
										Cras eu mauris feugiat, suscipit velit eget, ullamcorper ipsum  libero neque, sit amet.
									</p>
								</div><!-- .pad-20 -->
							</div><!-- .swipe-item -->

							<div class="swipe-item">
								<div class="pad-20">
									<img src="../assets/images/temp/airport.png" alt="st. john's airport" class="aligncenter">

									<p>
										Donec dictum libero neque, sit amet semper velit dictum nec. Phasellus ac egestas orci. 
										Cras eu mauris feugiat, suscipit velit eget, ullamcorper ipsum  libero neque, sit amet semper. 
										Donec dictum libero neque, sit amet semper velit dictum nec. Phasellus ac egestas orci. 
										Cras eu mauris feugiat, suscipit velit eget, ullamcorper ipsum  libero neque, sit amet.
									</p>
								</div><!-- .pad-20 -->
							</div><!-- .swipe-item -->

							<div class="swipe-item">
								<div class="pad-20">
									<img src="../assets/images/temp/airport.png" alt="st. john's airport" class="aligncenter">

									<p>
										Donec dictum libero neque, sit amet semper velit dictum nec. Phasellus ac egestas orci. 
										Cras eu mauris feugiat, suscipit velit eget, ullamcorper ipsum  libero neque, sit amet semper. 
										Donec dictum libero neque, sit amet semper velit dictum nec. Phasellus ac egestas orci. 
										Cras eu mauris feugiat, suscipit velit eget, ullamcorper ipsum  libero neque, sit amet.
									</p>
								</div><!-- .pad-20 -->
							</div><!-- .swipe-item -->

						</div><!-- .swiper -->
					</div><!-- .swiper-wrapper -->

				</div><!-- .item -->
			</div><!-- .col -->
		</div><!-- .grid -->
	</section><!-- .nopad -->

	<section class="nopad">
		<div class="half-block img-left">
			<div class="half-block-bg lazybg" data-src="../assets/images/temp/split-block-work-single.jpg"></div>
			<div class="half-block-content">

				<p>
					John Hearn Architect are currently in the process of handling two separate expansions to their original airport design. 
					Passenger growth has continued to surpass projections, necessitating expansion of the province’s busiest airport. 
					This project continues the aesthetic of the previous redevelopment, while taking advantage of recent advances in 
					sustainable building design. 
				</p>

				<p>
					Maintaining full accessibility and functionality during this renovation and expansion process is a logistical challenge, 
					considering the amount of passengers and staff who use the airport every day					
				</p>

			</div><!-- .half-block-content -->
		</div><!-- .half-block -->		
	</section><!-- .nopad -->

	<section class="nopad">
		<div class="half-block half-block-swipers">
			<div class="half-block-bg">

				<div class="swiper-wrapper">

					<div class="the-process-control-holder">
						
						<h3 class="the-process-control-title">The Process</h3>

						<div class="the-process-controls">
							<span class="slick-move-to">concept</span>
							<span class="slick-move-to">wireframe</span>
							<span class="slick-move-to">design</span>
						</div><!-- .the-process-controls -->

					</div><!-- .the-process-control-holder -->

					<div class="swiper" id="process-image-swiper"
						data-arrows="false"
						data-as-nav-for="#process-content-swiper"
						data-update-lazy-images="true"
						data-fade="true">
						<div class="swipe-item">
							<div class="swipe-item-bg lazybg" data-src="../assets/images/temp/split-block-concept.jpg"></div>
						</div><!-- .swipe-item -->
						<div class="swipe-item">
							<div class="swipe-item-bg lazybg" data-src="../assets/images/temp/split-block-latest.jpg"></div>
						</div><!-- .swipe-item -->
						<div class="swipe-item">
							<div class="swipe-item-bg lazybg" data-src="../assets/images/temp/split-block-concept.jpg"></div>
						</div><!-- .swipe-item -->
					</div><!-- .swiper -->

				</div><!-- .swiper-wrapper -->

			</div><!-- .half-block-bg -->
			<div class="half-block-content">

				<div class="swiper-wrapper">

					<div class="swiper" id="process-content-swiper" 
						data-arrows="false"
						data-as-nav-for="#process-image-swiper">

						<div class="swipe-item">

							<span class="half-block-tag">The Concept</span>

							<p>
								Nam consequat volutpat posuere. Morbi dignissim libero lectus. Aenean tempus dictum convallis. 
								Aenean sed neque neque. Curabitur faucibus euismod arcu, id condimentum urna convallis eget. 
								Nullam euismod nibh ante, non volutpat nisl consequat id. Vestibulum vel efficitur odio. 
								In eleifend, orci ut auctor ultrices, magna lacus laoreet ligula, a congue nunc nisi at massa. 
								Curabitur vel finibus felis, at rutrum metus. Nam congue lacus id ipsum varius feugiat. 
								Quisque ac varius tortor. Vivamus ac ligula dictum, aliquam tortor non, volutpat massa.
							</p>

						</div><!-- .swipe-item -->

						<div class="swipe-item">

							<span class="half-block-tag">The Wireframe</span>

							<p>
								Nam consequat volutpat posuere. Morbi dignissim libero lectus. Aenean tempus dictum convallis. 
								Aenean sed neque neque. Curabitur faucibus euismod arcu, id condimentum urna convallis eget. 
								Nullam euismod nibh ante, non volutpat nisl consequat id. Vestibulum vel efficitur odio. 
								In eleifend, orci ut auctor ultrices, magna lacus laoreet ligula, a congue nunc nisi at massa. 
								Curabitur vel finibus felis, at rutrum metus. Nam congue lacus id ipsum varius feugiat. 
								Quisque ac varius tortor. Vivamus ac ligula dictum, aliquam tortor non, volutpat massa.
							</p>

						</div><!-- .swipe-item -->

						<div class="swipe-item">

							<span class="half-block-tag">The Design</span>

							<p>
								Nam consequat volutpat posuere. Morbi dignissim libero lectus. Aenean tempus dictum convallis. 
								Aenean sed neque neque. Curabitur faucibus euismod arcu, id condimentum urna convallis eget. 
								Nullam euismod nibh ante, non volutpat nisl consequat id. Vestibulum vel efficitur odio. 
								In eleifend, orci ut auctor ultrices, magna lacus laoreet ligula, a congue nunc nisi at massa. 
								Curabitur vel finibus felis, at rutrum metus. Nam congue lacus id ipsum varius feugiat. 
								Quisque ac varius tortor. Vivamus ac ligula dictum, aliquam tortor non, volutpat massa.
							</p>

						</div><!-- .swipe-item -->

					</div><!-- .swiper -->

				</div><!-- .swiper-wrapper -->

				

			</div><!-- .half-block-content -->
		</div><!-- .half-block -->		
	</section><!-- .nopad -->	

	<section>
		<div class="sw">
			
			<h3>Project Gallery</h3>

			<div class="grid gallery-grid landscape-gallery collapse-450">
				
				<div class="col col-3 sm-col-2">

					<!-- href points to full size image -->
					<a href="../assets/images/temp/footer-gallery/gal-1.jpg" class="item gallery-item bounce mpopup" data-title="Image #1 Title" data-gallery="gallery-1-slug">

						<!-- data-src points to thumbnail -->
						<div class="gallery-bg lazybg img" data-src="../assets/images/temp/footer-gallery/gal-1.jpg"></div>
					</a><!-- .gallery-item -->

				</div><!-- .col -->

				<div class="col col-3 sm-col-2">

					<!-- href points to full size image -->
					<a href="../assets/images/temp/footer-gallery/gal-2.jpg" class="item gallery-item bounce mpopup" data-title="Image #2 Title" data-gallery="gallery-1-slug">

						<!-- data-src points to thumbnail -->
						<div class="gallery-bg lazybg img" data-src="../assets/images/temp/footer-gallery/gal-2.jpg"></div>
					</a><!-- .gallery-item -->

				</div><!-- .col -->

				<div class="col col-3 sm-col-2">

					<!-- href points to full size image -->
					<a href="../assets/images/temp/footer-gallery/gal-3.jpg" class="item gallery-item bounce mpopup" data-title="Image #3 Title" data-gallery="gallery-1-slug">

						<!-- data-src points to thumbnail -->
						<div class="gallery-bg lazybg img" data-src="../assets/images/temp/footer-gallery/gal-3.jpg"></div>
					</a><!-- .gallery-item -->

				</div><!-- .col -->

				<div class="col col-3 sm-col-2">

					<!-- href points to full size image -->
					<a href="../assets/images/temp/footer-gallery/gal-4.jpg" class="item gallery-item bounce mpopup" data-title="Image #4 Title" data-gallery="gallery-1-slug">

						<!-- data-src points to thumbnail -->
						<div class="gallery-bg lazybg img" data-src="../assets/images/temp/footer-gallery/gal-4.jpg"></div>
					</a><!-- .gallery-item -->

				</div><!-- .col -->

				<div class="col col-3 sm-col-2">

					<!-- href points to full size image -->
					<a href="../assets/images/temp/footer-gallery/gal-5.jpg" class="item gallery-item bounce mpopup" data-title="Image #5 Title" data-gallery="gallery-1-slug">

						<!-- data-src points to thumbnail -->
						<div class="gallery-bg lazybg img" data-src="../assets/images/temp/footer-gallery/gal-5.jpg"></div>
					</a><!-- .gallery-item -->

				</div><!-- .col -->

				<div class="col col-3 sm-col-2">

					<!-- href points to full size image -->
					<a href="../assets/images/temp/footer-gallery/gal-6.jpg" class="item gallery-item bounce mpopup" data-title="Image #6 Title" data-gallery="gallery-1-slug">

						<!-- data-src points to thumbnail -->
						<div class="gallery-bg lazybg img" data-src="../assets/images/temp/footer-gallery/gal-6.jpg"></div>
					</a><!-- .gallery-item -->

				</div><!-- .col -->

			</div><!-- .gallery-grid -->

		</div><!-- .sw -->
	</section>

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>