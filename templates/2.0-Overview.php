<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero">
	<div class="lazybg hero-full-bg" data-src="../assets/images/temp/hero/hero-inner.jpg"></div>

	<div class="sw">
		<div class="hero-content">

			<h1 class="hero-content-title">Meet</h1>			
			<p>
				Donec dictum libero neque, sit amet semper velit dictum nec. Phasellus ac egestas orci. 
				Cras eu mauris feugiat, suscipit velit eget, ullamcorper ipsum libero neque, sit amet semper.
			</p>

		</div><!-- .hero-content -->
	</div><!-- .sw -->

</div><!-- .hero -->

<div class="body">
	<section class="nopad">
			<div class="ov-grid grid nopad">
				<div class="col col-2 sm-col-1">
					<a href="#" class="item ov-item h2 bounce">
						<div class="lazybg ov-item-bg img" data-src="../assets/images/temp/overview/block-1.jpg"></div>
						<span>Company</span>			
					</a>
				</div><!-- .col -->
				<div class="col col-2 sm-col-1">
					<a href="#" class="item ov-item h2 bounce">
						<div class="lazybg ov-item-bg img" data-src="../assets/images/temp/overview/block-2.jpg"></div>
						<span>Our Team</span>			
					</a>
				</div><!-- .col -->
				<div class="col col-4 sm-col-2 xs-col-1">
					<a href="#" class="item ov-item h1 bounce">
						<div class="lazybg ov-item-bg img" data-src="../assets/images/temp/overview/sm-block-1.jpg"></div>
						<span>Sustainability</span>			
					</a>
				</div><!-- .col -->		
				<div class="col col-4 sm-col-2 xs-col-1">
					<a href="#" class="item ov-item h1 bounce">
						<div class="lazybg ov-item-bg img" data-src="../assets/images/temp/overview/sm-block-2.jpg"></div>
						<span>Opportunities</span>			
					</a>
				</div><!-- .col -->	
				<div class="col col-2 sm-col-1">
					<a href="#" class="item ov-item h1 bounce">
						<div class="lazybg ov-item-bg img" data-src="../assets/images/temp/overview/sm-block-3.jpg"></div>
						<span>Contact Us</span>			
					</a>
				</div><!-- .col -->		
			</div><!-- .ov-grid -->
	</section><!-- .nopad -->	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>