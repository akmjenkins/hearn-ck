<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero">
	<div class="lazybg hero-full-bg" data-src="../assets/images/temp/hero/hero-work.jpg"></div>

	<div class="sw">
		<div class="hero-content">

			<h1 class="hero-content-title">News</h1>			
			<p>
				Donec dictum libero neque, sit amet semper velit dictum nec. Phasellus ac egestas orci. 
				Cras eu mauris feugiat, suscipit velit eget, ullamcorper ipsum libero neque, sit amet semper.
			</p>

		</div><!-- .hero-content -->
	</div><!-- .sw -->

</div><!-- .hero -->

<div class="body">

	<section class="nopad">
		<div class="sw">

			<div class="breadcrumbs">
				<a href="#" class="t-fa-abs fa-home">Home</a>
				<a href="#">The Latest</a>
				<a href="#">News</a>
			</div><!-- .breadcrumbs -->

		</div><!-- .sw -->
	</section><!-- .nopad -->

	<section class="nopad">
		
		<div class="half-block img-right">
			<div class="half-block-bg lazybg" data-src="../assets/images/temp/split-block-latest.jpg"></div>
			<div class="half-block-content">
				
				<span class="half-block-tag">Featured Post</span>

				<span class="half-block-title">St. John's International Airport</span>

				<p>
					Donec dictum libero neque, sit amet semper velit dictum nec. Phasellus ac egestas orci. 
					Cras eu mauris feugiat, suscipit velit eget, ullamcorper ipsum  libero neque, sit amet semper.
				</p>

				<a href="#" class="inline">Read More</a>

			</div><!-- .half-block-content -->
		</div><!-- .half-block -->

	</section><!-- .nopad -->

	<section class="nopad">

		<div class="filter-section">

			<div class="sw">

				<div class="filter-bar">
					<div class="filter-bar-content">

						<div class="filter-bar-left">
							<div class="count">
								<span class="num">10</span> articles found								
							</div><!-- .count -->
						</div><!-- .filter-bar-left -->

						<div class="filter-bar-meta">
							<div class="filter-controls">
								<button class="previous">Previous</button>
								<button class="next">Next</button>
							</div><!-- .filter-controls -->
						</div><!-- .filter-bar-meta -->

					</div><!-- .filter-bar-content -->
				</div><!-- .filter-bar -->

			</div><!-- .sw -->

			<div class="filter-content">

				<div class="grid card-grid eqh nopad collapse-900">	

					<div class="col col-2">
						<div class="item card-item dark-bg">

							<div class="card-item-bg lazybg" data-src="../assets/images/temp/cards/card-3.jpg"></div>
							<div class="card-item-content">
								<span class="card-item-title">Bruneau Centre for Research</span>
								<span class="card-item-subtitle">St. John's, Newfoundland &amp; Labrador</span>

								<a href="#" class="inline card-item-link">View Project</a>
							</div><!-- .card-item-content -->

						</div><!-- .item -->
					</div><!-- .col -->

					<div class="col col-2">
						<div class="item card-item dark-bg">

							<div class="card-item-bg lazybg" data-src="../assets/images/temp/cards/card-4.jpg"></div>
							<div class="card-item-content">
								<span class="card-item-title">MUN New Student Residences</span>
								<span class="card-item-subtitle">St. John's, Newfoundland &amp; Labrador</span>

								<a href="#" class="inline card-item-link">View Project</a>
							</div><!-- .card-item-content -->

						</div><!-- .item -->
					</div><!-- .col -->	

					<div class="col col-2">
						<div class="item card-item dark-bg">

							<div class="card-item-bg lazybg" data-src="../assets/images/temp/cards/card-5.jpg"></div>
							<div class="card-item-content">
								<span class="card-item-title">MUN Field House</span>
								<span class="card-item-subtitle">St. John's, Newfoundland &amp; Labrador</span>

								<a href="#" class="inline card-item-link">View Project</a>
							</div><!-- .card-item-content -->

						</div><!-- .item -->
					</div><!-- .col -->

					<div class="col col-2">
						<div class="item card-item dark-bg">

							<div class="card-item-bg lazybg" data-src="../assets/images/temp/cards/card-6.jpg"></div>
							<div class="card-item-content">
								<span class="card-item-title">St. John's Long Term Care Facility</span>
								<span class="card-item-subtitle">St. John's, Newfoundland &amp; Labrador</span>

								<a href="#" class="inline card-item-link">View Project</a>
							</div><!-- .card-item-content -->

						</div><!-- .item -->
					</div><!-- .col -->				

				</div><!-- .grid -->				
				
			</div><!-- .filter-content -->

		</div><!-- .filter-section -->

	</section>

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>