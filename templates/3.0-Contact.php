<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero">
	<div class="lazybg hero-full-bg" data-src="../assets/images/temp/hero/hero-inner.jpg"></div>

	<div class="sw">
		<div class="hero-content">

			<h1 class="hero-content-title">Contact</h1>			
			<p>
				Donec dictum libero neque, sit amet semper velit dictum nec. Phasellus ac egestas orci. 
				Cras eu mauris feugiat, suscipit velit eget, ullamcorper ipsum libero neque, sit amet semper.
			</p>

		</div><!-- .hero-content -->
	</div><!-- .sw -->

</div><!-- .hero -->

<div class="body">

	<section class="nopad">
		<div class="sw">

			<div class="breadcrumbs">
				<a href="#" class="t-fa-abs fa-home">Home</a>
				<a href="#">Meet</a>
				<a href="#">Contact</a>
			</div><!-- .breadcrumbs -->

		</div><!-- .sw -->
	</section><!-- .nopad -->

	<section>
		<div class="sw">
			<article>
				<div class="main-body">				
					<div class="content">
						<div class="article-body">
							
							<h2>Talk to us today</h2>

							<p>
								Fusce non pellentesque nisi. Sed tempor tortor eget ante congue, ullamcorper fringilla mi ultricies. 
								Etiam eget neque mattis, elementum nulla a, hendrerit dui. Curabitur sagittis, turpis eget cursus maximus.						
							</p>

							<form action="/" class="body-form">
								<div class="fieldset">
									
									<input type="text" name="name" placeholder="Full Name">
									<input type="email" name="email" placeholder="Email Address">
									<textarea name="message" placeholder="Message"></textarea>

									<button class="button primary fill">Submit</button>

								</div><!-- .fieldset -->
							</form><!-- .body-form -->

							<br>
							<br>

							<h2>Address</h2>

							<div class="grid eqh address-grid collapse-650">

								<div class="col col-3">
									<div class="item">
										<address>
											Suite 102, Bristol Court <br>
											125 Kelsey Drive <br>
											St. John's, NL A1B 0L2
										</address>
									</div><!-- .item -->
								</div><!-- .col -->

								<div class="col col-3">
									<div class="item pad-20 sm-pad-0">
										
										<span class="block">709 753 0222</span>
										<span class="block">709 753 0222</span>

									</div><!-- .item -->
								</div><!-- .col -->

								<div class="col col-3">
									<div class="item pad-20 sm-pad-0">
										<button class="button scroll-to-map primary fill">Location Map</button>
									</div><!-- .item -->
								</div><!-- .col -->

							</div><!-- .grid -->

						</div><!-- .article-body -->
					</div><!-- .content -->
					<aside class="sidebar half-sidebar">
						<?php include('inc/i-sidebar-gallery.php'); ?>
					</aside><!-- .sidebar -->
				</div><!-- .main-body -->
			</article>
		</div><!-- .sw -->
	</section>

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>