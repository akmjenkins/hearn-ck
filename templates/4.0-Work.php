<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero">
	<div class="lazybg hero-full-bg" data-src="../assets/images/temp/hero/hero-inner.jpg"></div>

	<div class="sw">
		<div class="hero-content">

			<h1 class="hero-content-title">Work</h1>			
			<p>
				Donec dictum libero neque, sit amet semper velit dictum nec. Phasellus ac egestas orci. 
				Cras eu mauris feugiat, suscipit velit eget, ullamcorper ipsum libero neque, sit amet semper.
			</p>

		</div><!-- .hero-content -->
	</div><!-- .sw -->

</div><!-- .hero -->

<div class="body">

	<section class="nopad">
		<div class="sw">

			<div class="breadcrumbs">
				<a href="#" class="t-fa-abs fa-home">Home</a>
				<a href="#">Work</a>
			</div><!-- .breadcrumbs -->

		</div><!-- .sw -->
	</section><!-- .nopad -->

	<section class="nopad dark-bg">
		<div class="sw">
			
			<div class="list-meta">

				<label class="list-meta-item">
					<input type="checkbox">
					<span>Type</span>
				</label><!-- .list-meta-item -->

				<label class="list-meta-item">
					<input type="checkbox">
					<span>Services</span>
				</label><!-- .list-meta-item -->				

				<label class="list-meta-item">
					<input type="checkbox">
					<span>Most Viewed</span>
				</label><!-- .list-meta-item -->

				<label class="list-meta-item">
					<input type="checkbox">
					<span>Most Recent</span>
				</label><!-- .list-meta-item -->				

			</div><!-- .list-meta -->

		</div><!-- .sw -->
	</section>	

	<section class="nopad">
		
		<div class="half-block img-right">
			<div class="half-block-bg lazybg" data-src="../assets/images/temp/split-block-latest.jpg"></div>
			<div class="half-block-content">
				
				<span class="half-block-tag">Featured</span>

				<span class="half-block-title">St. John's International Airport</span>

				<p>
					Donec dictum libero neque, sit amet semper velit dictum nec. Phasellus ac egestas orci. 
					Cras eu mauris feugiat, suscipit velit eget, ullamcorper ipsum  libero neque, sit amet semper.
				</p>

				<a href="#" class="inline">View Project</a>

			</div><!-- .half-block-content -->
		</div><!-- .half-block -->

		<div class="grid card-grid eqh nopad collapse-900">

			<div class="col col-2">
				<div class="item card-item dark-bg">

					<div class="card-item-bg lazybg" data-src="../assets/images/temp/cards/card-1.jpg"></div>
					<div class="card-item-content">
						<span class="card-item-title">Corner Brook City Hall &amp; Library</span>
						<span class="card-item-subtitle">Corner Brook, Newfoundland &amp; Labrador</span>

						<a href="#" class="inline card-item-link">View Project</a>
					</div><!-- .card-item-content -->

				</div><!-- .item -->
			</div><!-- .col -->

			<div class="col col-2">
				<div class="item card-item dark-bg">

					<div class="card-item-bg lazybg" data-src="../assets/images/temp/cards/card-2.jpg"></div>
					<div class="card-item-content">
						<span class="card-item-title">Deer Lake Regional Airport</span>
						<span class="card-item-subtitle">Deer Lake, Newfoundland &amp; Labrador</span>

						<a href="#" class="inline card-item-link">View Project</a>
					</div><!-- .card-item-content -->

				</div><!-- .item -->
			</div><!-- .col -->		

			<div class="col col-2">
				<div class="item card-item dark-bg">

					<div class="card-item-bg lazybg" data-src="../assets/images/temp/cards/card-3.jpg"></div>
					<div class="card-item-content">
						<span class="card-item-title">Bruneau Centre for Research</span>
						<span class="card-item-subtitle">St. John's, Newfoundland &amp; Labrador</span>

						<a href="#" class="inline card-item-link">View Project</a>
					</div><!-- .card-item-content -->

				</div><!-- .item -->
			</div><!-- .col -->

			<div class="col col-2">
				<div class="item card-item dark-bg">

					<div class="card-item-bg lazybg" data-src="../assets/images/temp/cards/card-4.jpg"></div>
					<div class="card-item-content">
						<span class="card-item-title">MUN New Student Residences</span>
						<span class="card-item-subtitle">St. John's, Newfoundland &amp; Labrador</span>

						<a href="#" class="inline card-item-link">View Project</a>
					</div><!-- .card-item-content -->

				</div><!-- .item -->
			</div><!-- .col -->	

			<div class="col col-2">
				<div class="item card-item dark-bg">

					<div class="card-item-bg lazybg" data-src="../assets/images/temp/cards/card-5.jpg"></div>
					<div class="card-item-content">
						<span class="card-item-title">MUN Field House</span>
						<span class="card-item-subtitle">St. John's, Newfoundland &amp; Labrador</span>

						<a href="#" class="inline card-item-link">View Project</a>
					</div><!-- .card-item-content -->

				</div><!-- .item -->
			</div><!-- .col -->

			<div class="col col-2">
				<div class="item card-item dark-bg">

					<div class="card-item-bg lazybg" data-src="../assets/images/temp/cards/card-6.jpg"></div>
					<div class="card-item-content">
						<span class="card-item-title">St. John's Long Term Care Facility</span>
						<span class="card-item-subtitle">St. John's, Newfoundland &amp; Labrador</span>

						<a href="#" class="inline card-item-link">View Project</a>
					</div><!-- .card-item-content -->

				</div><!-- .item -->
			</div><!-- .col -->				

		</div><!-- .grid -->

	</section>

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>