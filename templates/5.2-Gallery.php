<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero">
	<div class="lazybg hero-full-bg" data-src="../assets/images/temp/hero/hero-work.jpg"></div>

	<div class="sw">
		<div class="hero-content">

			<h1 class="hero-content-title">Gallery</h1>			
			<p>
				Donec dictum libero neque, sit amet semper velit dictum nec. Phasellus ac egestas orci. 
				Cras eu mauris feugiat, suscipit velit eget, ullamcorper ipsum libero neque, sit amet semper.
			</p>

		</div><!-- .hero-content -->
	</div><!-- .sw -->

</div><!-- .hero -->

<div class="body">

	<section class="nopad">
		<div class="sw">

			<div class="breadcrumbs">
				<a href="#" class="t-fa-abs fa-home">Home</a>
				<a href="#">Gallery</a>
			</div><!-- .breadcrumbs -->

		</div><!-- .sw -->
	</section><!-- .nopad -->

	<section class="nopad dark-bg">
		<div class="sw">
			
			<div class="list-meta">
				
				<div class="list-meta-item">
					<div class="selector with-arrow">
						<select name="">
							<option value="">Project</option>

							<!-- use data-tag to keep the displayed name short -->
							<option value="1" data-tag="Bruneau Centre">Bruneau Centre for Research</option>
							<option value="1" data-tag="Corner Brook...">Corner Brook City Hall &amp; Library</option>
						</select>
						<span class="value">&nbsp;</span>
					</div><!-- .selector -->
				</div><!-- .list-meta-item -->

				<div class="list-meta-item">
					<div class="selector with-arrow">
						<select name="">
							<option value="">Year</option>
							<option value="2010">2010</option>
							<option value="2011">2011</option>
							<option value="2012">2012</option>
							<option value="2013">2013</option>
						</select>
						<span class="value">&nbsp;</span>
					</div><!-- .selector -->
				</div><!-- .list-meta-item -->

				<label class="list-meta-item">
					<input type="checkbox">
					<span>Most Viewed</span>
				</label><!-- .list-meta-item -->

				<label class="list-meta-item">
					<input type="checkbox">
					<span>Most Recent</span>
				</label><!-- .list-meta-item -->				

			</div><!-- .list-meta -->

		</div><!-- .sw -->
	</section>

	<section>
		<div class="sw">
			
			<div class="gallery-section filter-section">
				
				<div class="gallery-header">

					<div class="gallery-title-wrap">
						<span class="gallery-title">Bruneau Centre for Research</span>
						<time datetime="2015-04-21">April 21, 2015</time>
					</div>					

					<div class="filter-controls">
						<button class="previous">Previous</button>
						<button class="next">Next</button>
					</div><!-- .filter-controls -->					

				</div><!-- .gallery-header -->

				<div class="filter-content">
					
					<div class="grid gallery-grid collapse-450">

						<div class="col lg-col-4 md-col-3 sm-col-2">

							<!-- href points to full size image -->
							<a href="../assets/images/temp/gallery-page/gal-1.jpg" class="item gallery-item bounce mpopup" data-title="Image #1 Title" data-gallery="gallery-1-slug">

								<!-- data-src points to thumbnail -->
								<div class="gallery-bg lazybg img" data-src="../assets/images/temp/gallery-page/gal-1.jpg"></div>
							</a><!-- .gallery-item -->

						</div><!-- .col -->

						<div class="col lg-col-4 md-col-3 sm-col-2">
							<a href="../assets/images/temp/gallery-page/gal-2.jpg" class="item gallery-item bounce mpopup" data-title="Image #2 Title" data-gallery="gallery-1-slug">
								<div class="gallery-bg lazybg img" data-src="../assets/images/temp/gallery-page/gal-2.jpg"></div>
							</a><!-- .gallery-item -->
						</div><!-- .col -->

						<div class="col lg-col-4 md-col-3 sm-col-2">
							<a href="../assets/images/temp/gallery-page/gal-3.jpg" class="item gallery-item bounce mpopup" data-title="Image #3 Title" data-gallery="gallery-1-slug">
								<div class="gallery-bg lazybg img" data-src="../assets/images/temp/gallery-page/gal-3.jpg"></div>
							</a><!-- .gallery-item -->
						</div><!-- .col -->

						<div class="col lg-col-4 md-col-3 sm-col-2">
							<a href="../assets/images/temp/gallery-page/gal-4.jpg" class="item gallery-item bounce mpopup" data-title="Image #4 Title" data-gallery="gallery-1-slug">
								<div class="gallery-bg lazybg img" data-src="../assets/images/temp/gallery-page/gal-4.jpg"></div>
							</a><!-- .gallery-item -->
						</div><!-- .col -->

						<div class="col lg-col-4 md-col-3 sm-col-2">
							<a href="../assets/images/temp/gallery-page/gal-5.jpg" class="item gallery-item bounce mpopup" data-title="Image #5 Title" data-gallery="gallery-1-slug">
								<div class="gallery-bg lazybg img" data-src="../assets/images/temp/gallery-page/gal-5.jpg"></div>
							</a><!-- .gallery-item -->
						</div><!-- .col -->

						<div class="col lg-col-4 md-col-3 sm-col-2">
							<a href="../assets/images/temp/gallery-page/gal-6.jpg" class="item gallery-item bounce mpopup" data-title="Image #6 Title" data-gallery="gallery-1-slug">
								<div class="gallery-bg lazybg img" data-src="../assets/images/temp/gallery-page/gal-6.jpg"></div>
							</a><!-- .gallery-item -->
						</div><!-- .col -->

						<div class="col lg-col-4 md-col-3 sm-col-2">
							<a href="../assets/images/temp/gallery-page/gal-7.jpg" class="item gallery-item bounce mpopup" data-title="Image #7 Title" data-gallery="gallery-1-slug">
								<div class="gallery-bg lazybg img" data-src="../assets/images/temp/gallery-page/gal-7.jpg"></div>
							</a><!-- .gallery-item -->
						</div><!-- .col -->

						<div class="col lg-col-4 md-col-3 sm-col-2">
							<a href="../assets/images/temp/gallery-page/gal-8.jpg" class="item gallery-item bounce mpopup" data-title="Image #8 Title" data-gallery="gallery-1-slug">
								<div class="gallery-bg lazybg img" data-src="../assets/images/temp/gallery-page/gal-8.jpg"></div>
							</a><!-- .gallery-item -->
						</div><!-- .col -->				

					</div><!-- .grid.gallery-grid -->

				</div><!-- .filter-content -->

			</div><!-- .filter-section -->

		</div><!-- .sw -->
	</section>

	<section>
		<div class="sw">
			
			<div class="gallery-section filter-section">
				
				<div class="gallery-header">

					<div class="gallery-title-wrap">
						<span class="gallery-title">Corner Brook City Hall &amp; Library</span>
						<time datetime="2015-04-21">April 21, 2015</time>
					</div>					

					<div class="filter-controls">
						<button class="previous">Previous</button>
						<button class="next">Next</button>
					</div><!-- .filter-controls -->					

				</div><!-- .gallery-header -->

				<div class="filter-content">
					
					<div class="grid gallery-grid collapse-450">

						<div class="col lg-col-4 md-col-3 sm-col-2">

							<!-- href points to full size image -->
							<a href="../assets/images/temp/gallery-page/gal-9.jpg" class="item gallery-item bounce mpopup" data-title="Image #9 Title" data-gallery="gallery-2-slug">

								<!-- data-src points to thumbnail -->
								<div class="gallery-bg lazybg img" data-src="../assets/images/temp/gallery-page/gal-9.jpg"></div>
							</a><!-- .gallery-item -->

						</div><!-- .col -->

						<div class="col lg-col-4 md-col-3 sm-col-2">
							<a href="../assets/images/temp/gallery-page/gal-10.jpg" class="item gallery-item bounce mpopup" data-title="Image #10 Title" data-gallery="gallery-2-slug">
								<div class="gallery-bg lazybg img" data-src="../assets/images/temp/gallery-page/gal-10.jpg"></div>
							</a><!-- .gallery-item -->
						</div><!-- .col -->

						<div class="col lg-col-4 md-col-3 sm-col-2">
							<a href="../assets/images/temp/gallery-page/gal-11.jpg" class="item gallery-item bounce mpopup" data-title="Image #11 Title" data-gallery="gallery-3-slug">
								<div class="gallery-bg lazybg img" data-src="../assets/images/temp/gallery-page/gal-11.jpg"></div>
							</a><!-- .gallery-item -->
						</div><!-- .col -->

						<div class="col lg-col-4 md-col-3 sm-col-2">
							<a href="../assets/images/temp/gallery-page/gal-12.jpg" class="item gallery-item bounce mpopup" data-title="Image #12 Title" data-gallery="gallery-4-slug">
								<div class="gallery-bg lazybg img" data-src="../assets/images/temp/gallery-page/gal-12.jpg"></div>
							</a><!-- .gallery-item -->
						</div><!-- .col -->

						<div class="col lg-col-4 md-col-3 sm-col-2">
							<a href="../assets/images/temp/gallery-page/gal-13.jpg" class="item gallery-item bounce mpopup" data-title="Image #13 Title" data-gallery="gallery-5-slug">
								<div class="gallery-bg lazybg img" data-src="../assets/images/temp/gallery-page/gal-13.jpg"></div>
							</a><!-- .gallery-item -->
						</div><!-- .col -->

						<div class="col lg-col-4 md-col-3 sm-col-2">
							<a href="../assets/images/temp/gallery-page/gal-14.jpg" class="item gallery-item bounce mpopup" data-title="Image #14 Title" data-gallery="gallery-6-slug">
								<div class="gallery-bg lazybg img" data-src="../assets/images/temp/gallery-page/gal-14.jpg"></div>
							</a><!-- .gallery-item -->
						</div><!-- .col -->

						<div class="col lg-col-4 md-col-3 sm-col-2">
							<a href="../assets/images/temp/gallery-page/gal-15.jpg" class="item gallery-item bounce mpopup" data-title="Image #15 Title" data-gallery="gallery-7-slug">
								<div class="gallery-bg lazybg img" data-src="../assets/images/temp/gallery-page/gal-15.jpg"></div>
							</a><!-- .gallery-item -->
						</div><!-- .col -->

						<div class="col lg-col-4 md-col-3 sm-col-2">
							<a href="../assets/images/temp/gallery-page/gal-16.jpg" class="item gallery-item bounce mpopup" data-title="Image #16 Title" data-gallery="gallery-8-slug">
								<div class="gallery-bg lazybg img" data-src="../assets/images/temp/gallery-page/gal-16.jpg"></div>
							</a><!-- .gallery-item -->
						</div><!-- .col -->				

					</div><!-- .grid.gallery-grid -->

				</div><!-- .filter-content -->

			</div><!-- .filter-section -->

		</div><!-- .sw -->
	</section>	

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>