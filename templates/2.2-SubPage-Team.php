<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero">
	<div class="lazybg hero-full-bg" data-src="../assets/images/temp/hero/hero-team.jpg"></div>

	<div class="sw">
		<div class="hero-content">

			<h1 class="hero-content-title">Our Team.</h1>			
			<p>
				Donec dictum libero neque, sit amet semper velit dictum nec. Phasellus ac egestas orci. 
				Cras eu mauris feugiat, suscipit velit eget, ullamcorper ipsum libero neque, sit amet semper.
			</p>

		</div><!-- .hero-content -->
	</div><!-- .sw -->

</div><!-- .hero -->

<div class="body">

	<section class="nopad">
		<div class="sw">
			<div class="breadcrumbs">
				<a href="#" class="t-fa-abs fa-home">Home</a>
				<a href="#">Meet</a>
				<a href="#">Our Team</a>
			</div><!-- .breadcrumbs -->	
		</div><!-- .sw -->
	</section><!-- .nopad -->

	<section>
		<div class="sw">

			<div class="grid centered">

				<div class="col lg-col-4 md-col-3 sm-col-2 xs-col-1">
					<div class="item">
						
						<div class="tm lazybg" data-src="../assets/images/temp/team/tm-1.jpg">
							<div class="tm-info">
								<span class="tm-name">Person Name</spam>
								<span class="tm-pos">Person Position</span>
							</div><!-- .tm-info -->
						</div><!-- .tm -->

					</div><!-- .item -->
				</div><!-- .col -->

				<div class="col lg-col-4 md-col-3 sm-col-2 xs-col-1">
					<div class="item">
						
						<div class="tm lazybg" data-src="../assets/images/temp/team/tm-2.jpg">
							<div class="tm-info">
								<span class="tm-name">Person Name</spam>
								<span class="tm-pos">Person Position</span>
							</div><!-- .tm-info -->
						</div><!-- .tm -->

					</div><!-- .item -->
				</div><!-- .col -->		

				<div class="col lg-col-4 md-col-3 sm-col-2 xs-col-1">
					<div class="item">
						
						<div class="tm lazybg" data-src="../assets/images/temp/team/tm-3.jpg">
							<div class="tm-info">
								<span class="tm-name">Person Name</spam>
								<span class="tm-pos">Person Position</span>
							</div><!-- .tm-info -->
						</div><!-- .tm -->

					</div><!-- .item -->
				</div><!-- .col -->	

				<div class="col lg-col-4 md-col-3 sm-col-2 xs-col-1">
					<div class="item">
						
						<div class="tm lazybg" data-src="../assets/images/temp/team/tm-4.jpg">
							<div class="tm-info">
								<span class="tm-name">Person Name</spam>
								<span class="tm-pos">Person Position</span>
							</div><!-- .tm-info -->
						</div><!-- .tm -->

					</div><!-- .item -->
				</div><!-- .col -->

			</div><!-- .grid -->
		</div><!-- .sw -->
	</section>

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>