<div class="mobile-nav-bg"></div>

<div class="nav-wrap" tabindex="-1">

	<button class="meta-button fa-times-circle toggle-nav">Close</button>
	
	<nav>
		<ul>
			<li><a href="#">Meet</a></li>
			<li><a href="#">Work</a></li>
			<li><a href="#">Services</a></li>
			<li><a href="#">Gallery</a></li>
			<li><a href="#">The Latest</a></li>
		</ul>
	</nav>

	<div class="swiper-wrapper nav-swiper-wrapper">
		<div class="swiper"
			data-arrows="false">

			<div class="swipe-item nav-swipe-item">
				
				
				<a href="#" class="lazybg nav-swipe-item-img" data-src="../assets/images/temp/menu/menu-meet.jpg">
					<span>Meet</span>
				</a>

				<span class="nav-swipe-item-title">Creating architecture that blends in by standing out.</span>

				<p class="nav-swipe-item-content">
					Aliquam erat volutpat. Fusce in nisi non massa volutpat imperdiet. Aliquam dictum at magna at faucibus. 
					Proin ut luctus eros, vitae accumsan augue. Pellentesque vel ligula bibendum, vestibulum risus dignissim, sodales ante.					
				</p>

			</div><!-- .swipe-item nav-swipe-item -->

			<div class="swipe-item nav-swipe-item">
				
				
				<a href="#" class="lazybg nav-swipe-item-img" data-src="../assets/images/temp/menu/menu-meet.jpg">
					<span>Work</span>
				</a>

				<span class="nav-swipe-item-title">Creating architecture that blends in by standing out.</span>

				<p class="nav-swipe-item-content">
					Aliquam erat volutpat. Fusce in nisi non massa volutpat imperdiet. Aliquam dictum at magna at faucibus. 
					Proin ut luctus eros, vitae accumsan augue. Pellentesque vel ligula bibendum, vestibulum risus dignissim, sodales ante.					
				</p>

			</div><!-- .swipe-item nav-swipe-item -->	

			<div class="swipe-item nav-swipe-item">
				
				
				<a href="#" class="lazybg nav-swipe-item-img" data-src="../assets/images/temp/menu/menu-meet.jpg">
					<span>Services</span>
				</a>

				<span class="nav-swipe-item-title">Creating architecture that blends in by standing out.</span>

				<p class="nav-swipe-item-content">
					Aliquam erat volutpat. Fusce in nisi non massa volutpat imperdiet. Aliquam dictum at magna at faucibus. 
					Proin ut luctus eros, vitae accumsan augue. Pellentesque vel ligula bibendum, vestibulum risus dignissim, sodales ante.					
				</p>

			</div><!-- .swipe-item nav-swipe-item -->

			<div class="swipe-item nav-swipe-item">
				
				
				<a href="#" class="lazybg nav-swipe-item-img" data-src="../assets/images/temp/menu/menu-meet.jpg">
					<span>Gallery</span>
				</a>

				<span class="nav-swipe-item-title">Creating architecture that blends in by standing out.</span>

				<p class="nav-swipe-item-content">
					Aliquam erat volutpat. Fusce in nisi non massa volutpat imperdiet. Aliquam dictum at magna at faucibus. 
					Proin ut luctus eros, vitae accumsan augue. Pellentesque vel ligula bibendum, vestibulum risus dignissim, sodales ante.					
				</p>

			</div><!-- .swipe-item nav-swipe-item -->	

			<div class="swipe-item nav-swipe-item">
				
				
				<a href="#" class="lazybg nav-swipe-item-img" data-src="../assets/images/temp/menu/menu-meet.jpg">
					<span>The Latest</span>
				</a>

				<span class="nav-swipe-item-title">Creating architecture that blends in by standing out.</span>

				<p class="nav-swipe-item-content">
					Aliquam erat volutpat. Fusce in nisi non massa volutpat imperdiet. Aliquam dictum at magna at faucibus. 
					Proin ut luctus eros, vitae accumsan augue. Pellentesque vel ligula bibendum, vestibulum risus dignissim, sodales ante.					
				</p>

			</div><!-- .swipe-item nav-swipe-item -->

		</div><!-- .swiper -->
	</div><!-- .swiper-wrapper -->

	<?php include('i-social.php'); ?>

</div><!-- .nav-wrap -->