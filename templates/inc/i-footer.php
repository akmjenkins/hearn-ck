			<footer>
			
				<div class="sw">
					<div class="footer-row">
					
						<div class="footer-logo">
							<a href="/" class="lazybg with-img" data-src="../assets/images/hearn-logo.svg"></a>
						</div><!-- .footer-logo -->
						
						<div class="footer-nav">
							
							<ul>
								<li><a href="#">Meet</a></li>
								<li><a href="#">Work</a></li>
								<li><a href="#">Services</a></li>
								<li><a href="#">Gallery</a></li>
								<li><a href="#">The Latest</a></li>
							</ul>
							
						</div><!-- .footer-nav -->
						
						<div class="footer-social">
						
							<?php include('i-social.php'); ?>
						
						</div><!-- .footer-info -->
					
					</div><!-- .footer-row -->
				</div><!-- .sw -->

				<div class="footer-map-wrap">
					<span class="footer-map-arrow toggle-map">View Map</span>
					<div class="footer-map">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2692.4361562820495!2d-52.7814982!3d47.559301700000006!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4b0ca49eb8e9f169%3A0x9c14f42411318828!2s125+Kelsey+Dr%2C+St+John&#39;s%2C+NL+A1B!5e0!3m2!1sen!2sca!4v1431129054882" frameborder="0" style="border:0"></iframe>
					</div><!-- .footer-map -->
				</div>
				
				<div class="copyright">
					<div class="sw">
						<div class="copyright-box">
							Copyright &copy; <?php echo date('Y'); ?> <a href="/">John Hearn Architect</a>. All Rights Reserved.</li>
						</div>

						<div class="copyright-box">
							<address>
								Suite 102, Bristol Court | 125 Kelsey Drive | St. John's, NL A1B 0L2
							</address>
						</div><!-- .copyright-box -->
						
						<div class="copyright-box">
							<a href="http://jac.co" rel="external" title="JAC. We Create." id="jac"><img src="../assets/images/jac-logo.svg" alt="JAC Logo."></a>
						</div><!-- .copyright-box -->
					</div><!-- .sw -->
				</div><!-- .copyright -->
				
			</footer><!-- .footer -->

		</div><!-- .page-wrapper -->	

		<script>
			var templateJS = {
				templateURL: 'http://dev.me/hearn-ck',
				CACHE_BUSTER: '<?php echo time(); ?>'	/* cache buster - set this to some unique string whenever an update is performed on js/css files, or when an admin is logged in */
			};
		</script>

		<script src="../assets/js/min/main-min.js"></script>
	</body>
</html>