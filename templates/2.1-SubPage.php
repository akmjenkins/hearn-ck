<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero">
	<div class="lazybg hero-full-bg" data-src="../assets/images/temp/hero/hero-inner.jpg"></div>

	<div class="sw">
		<div class="hero-content">

			<h1 class="hero-content-title">Contact</h1>			
			<p>
				Donec dictum libero neque, sit amet semper velit dictum nec. Phasellus ac egestas orci. 
				Cras eu mauris feugiat, suscipit velit eget, ullamcorper ipsum libero neque, sit amet semper.
			</p>

		</div><!-- .hero-content -->
	</div><!-- .sw -->

</div><!-- .hero -->

<div class="body">

	<section class="nopad">
		<div class="sw">
			<div class="breadcrumbs">
				<a href="#" class="t-fa-abs fa-home">Home</a>
				<a href="#">Meet</a>
				<a href="#">Contact</a>
			</div><!-- .breadcrumbs -->
		</div><!-- .sw -->
	</section><!-- .nopad -->


	<section>
		<div class="sw">
			<article>

				<div class="main-body">				
					<div class="content">
						<div class="article-body">
							
							<h2>Originality</h2>


							<p>
								Donec dictum libero neque, sit amet semper velit dictum nec. Phasellus ac egestas orci. 
								Cras eu mauris feugiat, suscipit velit eget, ullamcorper ipsum  libero neque, sit amet 
								semper. Donec dictum libero neque, sit amet semper velit dictum nec. Phasellus ac egestas 
								orci. Cras eu mauris feugiat, suscipit velit eget, ullamcorper ipsum  libero neque, sit 
								amet semper.Donec dictum libero neque, sit amet semper velit dictum nec. Phasellus ac egestas 
								orci. Cras eu mauris feugiat, suscipit velit eget, ullamcorper ipsum  libero neque, sit 
								amet semper.
							</p>
		 
							<p>
								Donec dictum libero neqe, sit amet semper velit dictum nec. Phasellus ac egestas orci. Cras eu 
								mauris feugiat, suscipit velit eget, ullamcorper ipsum  libero neque, sit amet semper. Donec 
								dictum libero neque, sit amet semper velit dictum nec. Phasellus ac egestas orci. Cras eu mauris 
								feugiat, suscipit velit eget, ullamcorper ipsum  libero neque, sit amet semper.Donec dictum 
								libero neque, sit amet semper velit dictum nec. Phasellus ac egestas orci. Cras eu mauris feugiat, 
								suscipit velit eget, ullamcorper ipsum  libero neque, sit amet semper.
							</p>

							<p>
								Maecenas bibendum mattis aliquam. Cras sit amet elit ut mauris ultricies molestie. Pellentesque 
								habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Etiam pellentesque 
								tincidunt volutpat. Donec sed risus nec sem imperdiet venenatis nec nec justo.
							</p>
							

						</div><!-- .article-body -->
					</div><!-- .content -->
					<aside class="sidebar half-sidebar">
						<?php include('inc/i-sidebar-gallery.php'); ?>
					</aside><!-- .sidebar -->
				</div><!-- .main-body -->
			</article>
		</div><!-- .sw -->
	</section>

	<section class="full-bg lazybg" data-src="../assets/images/temp/footer-mask.jpg">
	</section><!-- .full-bg -->

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>